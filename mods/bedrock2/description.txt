Adds an indestructable bedrock layer at the bottom of the world.

Mod origin:
git: git://repo.or.cz/minetest_bedrock2.git
forum: https://forum.minetest.net/viewtopic.php?f=11&t=11271

Changes applied to suit Zelar Subgame:
* removed unnecessary png's;
* updated license to MIT;
* removed option to set bedrock height from UI;
