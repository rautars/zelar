-------------------------------
-- Zelar debug

-- FOR DEBUG / TEST PURPOSE ONLY

-- License: MIT
-- Credits: xeranas
-------------------------------

local noise_obj_temp
local noise_obj_humid

local np_temp = {
	offset = 50,
	scale = 50,
	spread = {x = 1000, y = 1000, z = 1000},
	seed = 5349,
	octaves = 3,
	persist = 0.5,
	lacunarity = 2.0
}

local np_humid = {
	offset = 50,
	scale = 50,
	spread = {x = 1000, y = 1000, z = 1000},
	seed = 842,
	octaves = 3,
	persist = 0.5,
	lacunarity = 2.0
}

-- For quick look around world added fast travel privileges
minetest.register_on_joinplayer(function(player)
	local playername = "singleplayer"
	local privs = minetest.get_player_privs(playername)
	privs.fly = true
	privs.fast = true
	privs.noclip = true
	privs.settime = true
	minetest.set_player_privs(playername, privs)

	noise_obj_temp = minetest.get_perlin(np_temp)
	noise_obj_humid = minetest.get_perlin(np_humid)
end)

local interval = 2
local total_dtime = 0

minetest.register_globalstep(function(dtime)
	total_dtime = total_dtime + dtime
	if total_dtime < interval then
		return
	end
	total_dtime = 0

	for i, player in ipairs(minetest.get_connected_players()) do
		local ppos = player:getpos()
		local noise_temp = noise_obj_temp:get2d({x = ppos.x, y = ppos.z})
		local noise_humid = noise_obj_humid:get2d({x = ppos.x, y = ppos.z})
		minetest.chat_send_all("Noise temp: " .. noise_temp .. " humid: " .. noise_humid)
	end
end)