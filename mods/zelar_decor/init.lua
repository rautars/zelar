-------------------------------
-- Zelar decor

-- License: MIT
-- Credits: xeranas
-------------------------------

local modpath = minetest.get_modpath("zelar_decor");
dofile(modpath.."/zelar_decor.lua")