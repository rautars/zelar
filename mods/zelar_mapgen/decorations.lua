-------------------------------
-- Zelar mapgen decorations

-- License: MIT
-- Credits: xeranas
-------------------------------

minetest.register_decoration({
	deco_type = "schematic",
	place_on = {"zelar_nodes:ice"},
	sidelen = 16,
	noise_params = {
		offset = 0.036,
		scale = 1.0022,
		spread = {x = 120, y = 120, z = 120},
		seed = 2,
		octaves = 4,
		persist = 0.77
	},
	biomes = {"iceland"},
	y_min = -10,
	y_max = 200,
	schematic = minetest.get_modpath("zelar_mapgen") .. "/schematics/crystals/crystal1.mts",
	flags = "place_center_x, place_center_z",
	rotation = "random",
})

minetest.register_decoration({
	deco_type = "schematic",
	place_on = {"zelar_nodes:ice"},
	sidelen = 16,
	noise_params = {
		offset = -0.0003,
		scale = 0.0009,
		spread = {x = 200, y = 200, z = 200},
		seed = 230,
		octaves = 3,
		persist = 0.6
	},
	biomes = {"iceland"},
	y_min = -10,
	y_max = 180,
	schematic = minetest.get_modpath("zelar_mapgen") .. "/schematics/crystals/crystal2.mts",
	flags = "place_center_x, place_center_z",
	rotation = "random",
})

minetest.register_decoration({
	deco_type = "schematic",
	place_on = {"zelar_nodes:stone"},
	sidelen = 16,
	noise_params = {
		offset = 0.0005,
		scale = 0.01,
		spread = {x = 200, y = 200, z = 200},
		seed = 430,
		octaves = 3,
		persist = 0.6
	},
	biomes = {"land"},
	y_min = -10,
	y_max = 200,
	schematic = minetest.get_modpath("zelar_mapgen") .. "/schematics/crystals/crystal3.mts",
	flags = "place_center_x, place_center_z",
	rotation = "random",
})