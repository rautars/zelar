-------------------------------
-- Zelar mapgen init

-- License: MIT
-- Credits: xeranas
-------------------------------

-- Only mapgen v7 is supported
local mg_name = minetest.get_mapgen_setting("mg_name")
if mg_name ~= "v7" then
	error("Unsupported mapgen error! Please use mapgen v7")
end

local modpath = minetest.get_modpath("zelar_mapgen");
dofile(modpath.."/zelar_mapgen.lua")
dofile(modpath.."/decorations.lua")
