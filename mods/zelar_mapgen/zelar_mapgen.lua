-------------------------------
-- edot mapgen

-- License: MIT
-- Credits: xeranas
-------------------------------

minetest.register_alias("mapgen_stone", "zelar_nodes:stone")
minetest.register_alias("mapgen_dirt", "zelar_nodes:stone")
minetest.register_alias("mapgen_dirt_with_grass", "zelar_nodes:stone")
minetest.register_alias("mapgen_dirt_with_snow", "zelar_nodes:stone")
minetest.register_alias("mapgen_sand", "zelar_nodes:redsand")
minetest.register_alias("mapgen_gravel", "zelar_nodes:stone")
minetest.register_alias("mapgen_desert_stone", "zelar_nodes:stone")
minetest.register_alias("mapgen_desert_sand", "zelar_nodes:redsand")
minetest.register_alias("mapgen_snowblock", "zelar_nodes:stone")
minetest.register_alias("mapgen_ice", "zelar_nodes:stone")
minetest.register_alias("mapgen_sandstone", "zelar_nodes:sand_stone")

minetest.register_alias("mapgen_snow", "zelar_nodes:stone")
minetest.register_alias("mapgen_tree", "air")
minetest.register_alias("mapgen_leaves", "air")
minetest.register_alias("mapgen_apple", "air")
minetest.register_alias("mapgen_jungletree", "air")
minetest.register_alias("mapgen_jungleleaves", "air")
minetest.register_alias("mapgen_junglegrass", "air")
minetest.register_alias("mapgen_pine_tree", "air")
minetest.register_alias("mapgen_pine_needles", "air")
minetest.register_alias("mapgen_cobble", "air")
minetest.register_alias("mapgen_stair_cobble", "air")
minetest.register_alias("mapgen_mossycobble", "air")
minetest.register_alias("mapgen_sandstonebrick", "air")
minetest.register_alias("mapgen_stair_sandstonebrick", "air")

minetest.register_alias("mapgen_river_water_source", "zelar_nodes:sand_stone")
minetest.register_alias("mapgen_water_source", "zelar_nodes:sand_stone")

minetest.register_alias("mapgen_lava_source", "zelar_nodes:stone")

local mgv7_shadow_limit = minetest.get_mapgen_setting("mgv7_shadow_limit") or 1024
local upper_limit = mgv7_shadow_limit - 1

minetest.clear_registered_biomes()

minetest.register_biome({
	name = "land",
	node_top = "zelar_nodes:stone",
	depth_top = 1,
	node_filler = "zelar_nodes:stone",
	depth_filler = 1,
	node_water = "air",
	node_riverbed = "zelar_nodes:redsand",
	depth_riverbed = 2,
	y_min = -200,
	y_max = 600,
	heat_point = 50,
	humidity_point = 35,
})

minetest.register_biome({
	name = "iceland",
	node_dust = "air",
	node_top = "zelar_nodes:ice",
	depth_top = 20,
	node_filler = "zelar_nodes:ice",
	depth_filler = 1,
	node_stone = "zelar_nodes:ice",
	node_water = "air",
	node_riverbed = "zelar_nodes:redsand",
	depth_riverbed = 1,
	y_min = -200,
	y_max = 600,
	heat_point = 0,
	humidity_point = 73,
})

minetest.register_biome({
	name = "redland",
	node_top = "zelar_nodes:redsand",
	depth_top = 2,
	node_filler = "zelar_nodes:sand_stone",
	depth_filler = 18,
	node_stone = "zelar_nodes:sand_stone",
	node_water = "air",
	node_riverbed = "zelar_nodes:redsand",
	depth_riverbed = 2,
	y_min = 5,
	y_max = 600,
	heat_point = 92,
	humidity_point = 16,
})

-- local mgv7_spflags = "mountains, ridges, floatlands"