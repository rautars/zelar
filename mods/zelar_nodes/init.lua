-------------------------------
-- Zelar nodes init

-- License: MIT
-- Credits: xeranas
-------------------------------

local modpath = minetest.get_modpath("zelar_nodes");
dofile(modpath.."/zelar_nodes.lua")
dofile(modpath.."/zelar_material.lua")