-------------------------------
-- Zelar material nodes

-- License: MIT
-- Credits: xeranas
-------------------------------

minetest.register_node("zelar_nodes:sand", {
	description = "Sand",
	tiles = {"zelar_nodes_sand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
	stack_max = 64,
})