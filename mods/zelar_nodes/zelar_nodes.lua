-------------------------------
-- Zelar nodes

-- License: MIT
-- Credits: xeranas
-------------------------------

minetest.register_node("zelar_nodes:cobble", {
	description = "Cobblestone",
	tiles = {"zelar_nodes_cobblestone.png"},
	is_ground_content = false,
	groups = {crumbly = 3, stone = 2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("zelar_nodes:crystal", {
	description = "Crystal",
	stack_max = 100,
	tiles = {"zelar_nodes_crystal.png"},
	is_ground_content = false,
	groups = {crumbly = 3},
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node("zelar_nodes:dirt", {
	description = "Dirt",
	tiles = {"zelar_nodes_dirt.png"},
	groups = {crumbly = 3, soil = 1},
	sounds = default.node_sound_dirt_defaults(),
})

minetest.register_node("zelar_nodes:dirt_with_grass", {
	description = "Dirt with Grass",
	tiles = {"zelar_nodes_grass_top.png", "zelar_nodes_dirt.png",
		{name = "zelar_nodes_dirt.png^zelar_nodes_grass_side.png",
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1, spreading_dirt_type = 1},
	drop = 'zelar_nodes:dirt',
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
})

minetest.register_node("zelar_nodes:gravel", {
	description = "Gravel",
	tiles = {"zelar_nodes_gravel.png"},
	groups = {crumbly = 2, falling_node = 1},
	sounds = default.node_sound_gravel_defaults(),
})

minetest.register_node("zelar_nodes:redsand", {
	description = "Red Sand",
	tiles = {"zelar_nodes_redsand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
})

minetest.register_node("zelar_nodes:sand_stone", {
	description = "Sand Stone",
	tiles = {"zelar_nodes_sandstone.png"},
	groups = {cracky = 3, stone = 1},
	drop = 'zelar_nodes:redsand',
	legacy_mineral = true,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("zelar_nodes:blue_crystal", {
	description = "Blue Crystal",
	tiles = {"zelar_nodes_blue_crystal.png"},
	groups = {crumbly = 3},
	light_source = 7,
	drop = 'zelar_nodes:blue_crystal',
	legacy_mineral = true,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("zelar_nodes:ice", {
	description = "Ice",
	tiles = {"zelar_nodes_ice.png"},
	is_ground_content = false,
	paramtype = "light",
	groups = {cracky = 3, puts_out_fire = 1, cools_lava = 1},
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_node("zelar_nodes:snowblock", {
	description = "Snow Block",
	tiles = {"zelar_nodes_snow.png"},
	groups = {crumbly = 3, puts_out_fire = 1, cools_lava = 1, snowy = 1},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_snow_footstep", gain = 0.15},
		dug = {name = "default_snow_footstep", gain = 0.2},
		dig = {name = "default_snow_footstep", gain = 0.2}
	}),

	on_construct = function(pos)
		pos.y = pos.y - 1
		if minetest.get_node(pos).name == "zelar_nodes:dirt_with_grass" then
			minetest.set_node(pos, {name = "zelar_nodes:dirt"})
		end
	end,
})

minetest.register_node("zelar_nodes:water_source", {
	description = "Water Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "zelar_nodes_water_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name = "zelar_nodes_water_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
			backface_culling = false,
		},
	},
	alpha = 160,
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "zelar_nodes:water_flowing",
	liquid_alternative_source = "zelar_nodes:water_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, puts_out_fire = 1, cools_lava = 1},
	sounds = default.node_sound_water_defaults(),
})

minetest.register_node("zelar_nodes:water_flowing", {
	description = "Flowing Water",
	drawtype = "flowingliquid",
	tiles = {"zelar_nodes_water.png"},
	special_tiles = {
		{
			name = "zelar_nodes_water_flowing_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
		{
			name = "zelar_nodes_water_flowing_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
	},
	alpha = 160,
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_flowing = "zelar_nodes:water_flowing",
	liquid_alternative_source = "zelar_nodes:water_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, puts_out_fire = 1,
		not_in_creative_inventory = 1, cools_lava = 1},
	sounds = default.node_sound_water_defaults(),
})

minetest.register_node("zelar_nodes:stone", {
	description = "Stone",
	tiles = {"zelar_nodes_stone.png"},
	groups = {cracky = 3, stone = 1},
	drop = 'zelar_nodes:cobble',
	legacy_mineral = true,
	sounds = default.node_sound_stone_defaults(),
})