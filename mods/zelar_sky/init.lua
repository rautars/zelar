-------------------------------
-- Zelar sky

-- License: MIT
-- Credits: xeranas
-------------------------------

local modpath = minetest.get_modpath("zelar_sky");
dofile(modpath.."/zelar_sky.lua")