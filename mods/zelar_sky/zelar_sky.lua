-------------------------------
-- Zelar sky

-- Description: controls visuals of zelar sky
-- License: MIT
-- Credits: xeranas
-------------------------------

local sl = {}
sl.layer_type = skylayer.SKY_PLAIN
sl.name = "rainbow_sky"
sl.data = {gradient_data={}}
sl.data.gradient_data.colors = {
	{r=0, g=0, b=0},
	{r=131, g=137, b=219},
	{r=171, g=242, b=140},
	{r=179, g=165, b=163},
	{r=191, g=191, b=191},
	{r=179, g=245, b=169},
	{r=173, g=139, b=254},
	{r=20, g=5, b=4}
}
sl.data.gradient_data.min_value = 0
sl.data.gradient_data.max_value = 1000

for k, player in ipairs(minetest.get_connected_players()) do
	skylayer.add_layer(player:get_player_name(), sl)
end

minetest.register_on_joinplayer(function(player)
	skylayer.add_layer(player:get_player_name(), sl)
end)
